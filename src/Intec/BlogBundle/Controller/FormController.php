<?php

namespace Intec\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FormController extends Controller
{
    public function indexAction()
    {

        return $this->render('IntecBlogBundle:Form:index.html.twig');
    }

    public function shortAction(Request $request)
    {
        $url1 = isset($_GET['url']) ? $_GET['url'] : NULL;
        $url = substr($url1, 0, strpos($url1, "/"));
        $url .= "/".substr(md5(rand(5, 10)), -5)/*"/ShortUrl"*/;

        if ($url1 == NULL) {
            $request->getSession()->getFlashBag()->add(
                'notice',
                'Enter a url'
            );
            return $this->redirect($this->generateUrl('intec_form_index'));
        }
        return $this->render('IntecBlogBundle:Form:short.html.twig', array('url' => $url));
    }
}
